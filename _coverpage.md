<!-- _coverpage.md -->

![logo](doc/show/logo.png)

# HBU-paperOline <small>1.1</small>

> 河北大学在线论文管理系统

- <b>后端</b> SpirngBoot | <b>前端</b> thymeleaf |  <b>数据库</b> MySQL-8.0
- 论文-加密 上传、下载
- MD5 验证文件一致性
- 集中权限管理（单点登录）
- 登录验证 角色控制
- <small> power-by 韩旭 2021年5月</small>

[系统使用文档](#HBU-paperOline)
[HBU-paperOline](#HBU-paperOline)


