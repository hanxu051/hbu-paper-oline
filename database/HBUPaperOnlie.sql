/*
 Navicat Premium Data Transfer

 Source Server         : phpstudy
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : ming

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 29/05/2021 17:03:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for upms_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `upms_dict_data`;
CREATE TABLE `upms_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_dict_data
-- ----------------------------

-- ----------------------------
-- Table structure for upms_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `upms_dict_type`;
CREATE TABLE `upms_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_dict_type
-- ----------------------------

-- ----------------------------
-- Table structure for upms_log
-- ----------------------------
DROP TABLE IF EXISTS `upms_log`;
CREATE TABLE `upms_log`  (
  `log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作描述',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作用户',
  `permissions` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限值',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `spend_time` int(11) NULL DEFAULT NULL COMMENT '消耗时间',
  `base_path` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '根路径',
  `uri` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'URI',
  `method` varchar(7) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求类型',
  `parameter` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `user_agent` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户标识',
  `ip` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  `result` tinyint(2) NULL DEFAULT NULL COMMENT '响应状态',
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 183 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_log
-- ----------------------------

-- ----------------------------
-- Table structure for upms_organization
-- ----------------------------
DROP TABLE IF EXISTS `upms_organization`;
CREATE TABLE `upms_organization`  (
  `organization_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '编号',
  `pid` int(10) NULL DEFAULT NULL COMMENT '所属上级',
  `name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织名称',
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织描述',
  `ctime` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`organization_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 107 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '组织' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_organization
-- ----------------------------
INSERT INTO `upms_organization` VALUES (1, 0, '河北大学', NULL, '2021-05-27 15:09:30');
INSERT INTO `upms_organization` VALUES (2, 13, '网络空间安全与计算机学院', NULL, '2021-05-27 15:09:30');
INSERT INTO `upms_organization` VALUES (3, 2, '信息安全专业', NULL, '2021-05-27 15:09:30');
INSERT INTO `upms_organization` VALUES (4, 2, '计算机科学与技术专业', NULL, '2021-05-27 15:09:30');
INSERT INTO `upms_organization` VALUES (5, 3, '信安1班', NULL, '2021-05-27 15:09:30');
INSERT INTO `upms_organization` VALUES (6, 3, '信安2班', NULL, '2021-05-27 15:09:30');
INSERT INTO `upms_organization` VALUES (7, 3, '信安3班', NULL, '2021-05-27 15:09:30');
INSERT INTO `upms_organization` VALUES (8, 3, '信安4班', NULL, '2021-05-27 15:09:30');
INSERT INTO `upms_organization` VALUES (9, 4, '计科1班', NULL, '2021-05-27 15:09:30');
INSERT INTO `upms_organization` VALUES (10, 4, '计科2班', NULL, '2021-05-27 15:09:30');
INSERT INTO `upms_organization` VALUES (11, 4, '计科3班', NULL, '2021-05-27 15:09:30');
INSERT INTO `upms_organization` VALUES (12, 4, '计科4班', NULL, '2021-05-27 15:09:30');
INSERT INTO `upms_organization` VALUES (13, 1, '河大新区', NULL, '2021-05-27 15:09:30');
INSERT INTO `upms_organization` VALUES (14, 1, '河大本部', NULL, '2021-05-27 15:09:30');
INSERT INTO `upms_organization` VALUES (15, 1, '河大医学部', NULL, '2021-05-27 15:09:30');

-- ----------------------------
-- Table structure for upms_permission
-- ----------------------------
DROP TABLE IF EXISTS `upms_permission`;
CREATE TABLE `upms_permission`  (
  `permission_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '编号',
  `system_id` int(10) UNSIGNED NOT NULL COMMENT '所属系统',
  `pid` int(10) NULL DEFAULT NULL COMMENT '所属上级',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `type` tinyint(4) NULL DEFAULT NULL COMMENT '类型(1:目录,2:菜单,3:按钮)',
  `permission_value` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限值',
  `uri` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路径',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态(0:禁止,1:正常)',
  `ctime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `orders` bigint(20) NOT NULL COMMENT '排序',
  PRIMARY KEY (`permission_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 112 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_permission
-- ----------------------------
INSERT INTO `upms_permission` VALUES (1, 1, 0, '系统资源管理', 1, '', '', 'fa fa-gear', 1, '2020-05-02 05:55:02', 1);
INSERT INTO `upms_permission` VALUES (2, 1, 1, '系统管理', 2, 'system:upmsSystem:read', '/system/upmsSystem', 'fa fa-wrench', 1, '2020-05-02 05:55:02', 1);
INSERT INTO `upms_permission` VALUES (3, 1, 1, '资源管理', 2, 'system:upmsPermission:read', '/system/upmsPermission', 'fa fa-navicon', 1, '2020-05-02 05:55:02', 2);
INSERT INTO `upms_permission` VALUES (4, 1, 0, '组织角色管理', 1, NULL, '', 'fa fa-users', 1, '2020-05-02 05:55:02', 4);
INSERT INTO `upms_permission` VALUES (5, 1, 4, '角色管理', 2, 'system:upmsRole:read', '/system/upmsRole', 'fa fa-vcard', 1, '2020-05-02 05:55:02', 5);
INSERT INTO `upms_permission` VALUES (6, 1, 4, '用户管理', 2, 'system:upmsUser:read', '/system/upmsUser', 'fa fa-user-circle', 1, '2020-05-02 05:55:02', 6);
INSERT INTO `upms_permission` VALUES (7, 1, 4, '组织管理', 2, 'system:upmsOrganization:read', '/system/upmsOrganization', 'fa fa-users', 1, '2020-05-02 05:55:02', 3);
INSERT INTO `upms_permission` VALUES (8, 1, 3, '修改', 3, 'system:upmsPermission:edit', '', '', 1, '2020-05-07 22:26:23', 1);
INSERT INTO `upms_permission` VALUES (9, 1, 3, '删除', 3, 'system:upmsPermission:remove', '', '', 1, '2020-05-07 22:34:02', 1);
INSERT INTO `upms_permission` VALUES (10, 1, 2, '添加', 3, 'system:upmsSystem:add', '', '', 1, '2020-05-07 22:44:41', 3);
INSERT INTO `upms_permission` VALUES (15, 1, 0, '日志监控管理', 1, '', '', 'fa fa-video-camera', 1, '2020-05-14 21:44:33', 10);
INSERT INTO `upms_permission` VALUES (16, 1, 15, '用户操作日志', 2, 'system:upmsLog:read', '/system/upmsLog', 'fa fa-tachometer', 1, '2020-05-14 21:46:41', 2);
INSERT INTO `upms_permission` VALUES (17, 1, 3, '添加', 3, 'system:upmsPermission:add', '', '', 1, '2020-05-14 23:32:57', 3);
INSERT INTO `upms_permission` VALUES (18, 1, 2, '修改', 3, 'system:upmsSystem:edit', '', '', 1, '2020-05-14 23:51:47', 3);
INSERT INTO `upms_permission` VALUES (19, 1, 5, '添加', 3, 'system:upmsRole:add', '', '', 1, '2020-05-14 23:54:46', 3);
INSERT INTO `upms_permission` VALUES (20, 1, 5, '修改', 3, 'system:upmsRole:edit', '', '', 1, '2020-05-14 23:55:16', 3);
INSERT INTO `upms_permission` VALUES (21, 1, 6, '添加', 3, 'system:upmsUser:add', '', '', 1, '2020-05-15 21:24:40', 3);
INSERT INTO `upms_permission` VALUES (22, 1, 6, '修改', 3, 'system:upmsUser:edit', '', '', 1, '2020-05-15 21:25:02', 3);
INSERT INTO `upms_permission` VALUES (23, 1, 7, '添加', 3, 'system:upmsOrganization:add', '', '', 1, '2020-05-15 21:25:34', 1);
INSERT INTO `upms_permission` VALUES (24, 1, 7, '修改', 3, 'system:upmsOrganization:edit', '', '', 1, '2020-05-15 21:27:10', 3);
INSERT INTO `upms_permission` VALUES (25, 1, 2, '删除', 3, 'system:upmsSystem:remove', '', '', 1, '2020-06-03 21:16:13', 3);
INSERT INTO `upms_permission` VALUES (26, 1, 7, '删除', 3, 'system:upmsOrganization:remove', '', '', 1, '2020-06-03 21:17:59', 3);
INSERT INTO `upms_permission` VALUES (27, 1, 5, '删除', 3, 'system:upmsRole:remove', '', '', 1, '2020-06-03 21:18:51', 3);
INSERT INTO `upms_permission` VALUES (28, 1, 6, '删除', 3, 'system:upmsUser:remove', '', '', 1, '2020-06-03 21:19:25', 3);
INSERT INTO `upms_permission` VALUES (29, 1, 16, '删除', 3, 'system:upmsLog:remove', '', '', 1, '2020-06-03 21:20:25', 1);
INSERT INTO `upms_permission` VALUES (30, 1, 15, '数据库监控', 2, '', '/druid/index', 'fa fa-bar-chart', 1, '2020-06-04 22:46:10', 2);
INSERT INTO `upms_permission` VALUES (31, 1, 15, 'API接口文档', 2, '', '/swagger-ui.html', 'fa fa-tags', 1, '2020-06-05 22:15:51', 3);
INSERT INTO `upms_permission` VALUES (100, 4, 0, '论文管理系统', 1, '', '', 'fa fa-graduation-cap', 1, '2021-05-26 21:20:55', 1);
INSERT INTO `upms_permission` VALUES (101, 4, 100, '论文管理中台', 2, 'system:upmsUploadFile:read', '/system/upmsUploadFile', 'fa fa-area-chart', 1, '2021-05-26 21:21:44', 1);
INSERT INTO `upms_permission` VALUES (102, 4, 101, '增加', 3, 'system:upmsUploadFile:add', '', '', 1, '2021-05-26 21:24:42', 1);
INSERT INTO `upms_permission` VALUES (104, 4, 101, '删除', 3, 'system:upmsUploadFile:batchRemove', '', '', 1, '2021-05-27 06:57:26', 3);
INSERT INTO `upms_permission` VALUES (105, 4, 101, '编辑', 3, 'system:upmsUploadFile:edit', '', '', 1, '2021-05-27 06:59:22', 5);
INSERT INTO `upms_permission` VALUES (106, 4, 100, '个人论文管理', 2, 'system:upmsUploadFile:read', '/system/upmsStuUploadFile', 'fa fa-calendar-check-o', 1, '2021-05-27 14:56:51', 1);
INSERT INTO `upms_permission` VALUES (107, 4, 106, '增加', 3, 'system:upmsUploadFile:add', '', '', 1, '2021-05-28 15:08:24', 1);
INSERT INTO `upms_permission` VALUES (108, 4, 106, '删除', 3, 'system:upmsUploadFile:batchRemove', '', '', 1, '2021-05-28 15:09:01', 1);
INSERT INTO `upms_permission` VALUES (109, 4, 106, '编辑', 3, 'system:upmsUploadFile:edit', '', '', 1, '2021-05-28 15:09:24', 1);
INSERT INTO `upms_permission` VALUES (110, 4, 0, '个人中心', 3, 'system:upmsUser:edit', '', '', 0, '2021-05-28 17:33:40', 1);
INSERT INTO `upms_permission` VALUES (111, 4, 106, '个人中心', 3, 'system:upmsUser:edit', '', '', 1, '2021-05-28 17:37:41', 1);

-- ----------------------------
-- Table structure for upms_role
-- ----------------------------
DROP TABLE IF EXISTS `upms_role`;
CREATE TABLE `upms_role`  (
  `role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色标题',
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `ctime` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `orders` bigint(20) NOT NULL COMMENT '排序',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_role
-- ----------------------------
INSERT INTO `upms_role` VALUES (1, 'super', '超级管理员', '拥有所有权限', '2020-05-02 05:55:02', 1);
INSERT INTO `upms_role` VALUES (2, 'admin', '管理员', '拥有除权限管理系统外的所有权限', '2020-05-02 05:55:02', 2);
INSERT INTO `upms_role` VALUES (6, 'test', '测试用户', '用于测试账号', '2020-05-01 21:55:02', 3);
INSERT INTO `upms_role` VALUES (100, 'teacher', '教师', '收学生论文', '2021-05-27 14:52:48', 1);
INSERT INTO `upms_role` VALUES (101, 'student', '学生', '学生上传论文', '2021-05-27 14:54:42', 1);

-- ----------------------------
-- Table structure for upms_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `upms_role_permission`;
CREATE TABLE `upms_role_permission`  (
  `role_permission_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '编号',
  `role_id` int(10) UNSIGNED NOT NULL COMMENT '角色编号',
  `permission_id` int(10) UNSIGNED NOT NULL COMMENT '权限编号',
  PRIMARY KEY (`role_permission_id`) USING BTREE,
  INDEX `FK_urp_role_id`(`role_id`) USING BTREE,
  INDEX `FK_urp_permission_id`(`permission_id`) USING BTREE,
  CONSTRAINT `FK_urp_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `upms_permission` (`permission_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_urp_role_id` FOREIGN KEY (`role_id`) REFERENCES `upms_role` (`role_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 693 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色权限关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_role_permission
-- ----------------------------
INSERT INTO `upms_role_permission` VALUES (227, 6, 1);
INSERT INTO `upms_role_permission` VALUES (228, 6, 2);
INSERT INTO `upms_role_permission` VALUES (229, 6, 10);
INSERT INTO `upms_role_permission` VALUES (230, 6, 18);
INSERT INTO `upms_role_permission` VALUES (231, 6, 3);
INSERT INTO `upms_role_permission` VALUES (232, 6, 8);
INSERT INTO `upms_role_permission` VALUES (233, 6, 17);
INSERT INTO `upms_role_permission` VALUES (303, 2, 100);
INSERT INTO `upms_role_permission` VALUES (304, 2, 101);
INSERT INTO `upms_role_permission` VALUES (305, 2, 4);
INSERT INTO `upms_role_permission` VALUES (306, 2, 7);
INSERT INTO `upms_role_permission` VALUES (307, 2, 23);
INSERT INTO `upms_role_permission` VALUES (308, 2, 24);
INSERT INTO `upms_role_permission` VALUES (309, 2, 26);
INSERT INTO `upms_role_permission` VALUES (310, 2, 5);
INSERT INTO `upms_role_permission` VALUES (311, 2, 19);
INSERT INTO `upms_role_permission` VALUES (312, 2, 20);
INSERT INTO `upms_role_permission` VALUES (313, 2, 27);
INSERT INTO `upms_role_permission` VALUES (314, 2, 6);
INSERT INTO `upms_role_permission` VALUES (315, 2, 21);
INSERT INTO `upms_role_permission` VALUES (316, 2, 22);
INSERT INTO `upms_role_permission` VALUES (317, 2, 28);
INSERT INTO `upms_role_permission` VALUES (318, 2, 15);
INSERT INTO `upms_role_permission` VALUES (319, 2, 16);
INSERT INTO `upms_role_permission` VALUES (320, 2, 29);
INSERT INTO `upms_role_permission` VALUES (639, 101, 100);
INSERT INTO `upms_role_permission` VALUES (640, 101, 106);
INSERT INTO `upms_role_permission` VALUES (641, 101, 107);
INSERT INTO `upms_role_permission` VALUES (642, 101, 108);
INSERT INTO `upms_role_permission` VALUES (643, 101, 109);
INSERT INTO `upms_role_permission` VALUES (644, 101, 111);
INSERT INTO `upms_role_permission` VALUES (645, 100, 100);
INSERT INTO `upms_role_permission` VALUES (646, 100, 101);
INSERT INTO `upms_role_permission` VALUES (647, 100, 102);
INSERT INTO `upms_role_permission` VALUES (648, 100, 104);
INSERT INTO `upms_role_permission` VALUES (649, 100, 105);
INSERT INTO `upms_role_permission` VALUES (650, 100, 106);
INSERT INTO `upms_role_permission` VALUES (651, 100, 107);
INSERT INTO `upms_role_permission` VALUES (652, 100, 108);
INSERT INTO `upms_role_permission` VALUES (653, 100, 109);
INSERT INTO `upms_role_permission` VALUES (654, 100, 111);
INSERT INTO `upms_role_permission` VALUES (655, 100, 4);
INSERT INTO `upms_role_permission` VALUES (656, 100, 6);
INSERT INTO `upms_role_permission` VALUES (657, 100, 21);
INSERT INTO `upms_role_permission` VALUES (658, 100, 22);
INSERT INTO `upms_role_permission` VALUES (659, 100, 28);
INSERT INTO `upms_role_permission` VALUES (660, 1, 1);
INSERT INTO `upms_role_permission` VALUES (661, 1, 2);
INSERT INTO `upms_role_permission` VALUES (662, 1, 10);
INSERT INTO `upms_role_permission` VALUES (663, 1, 18);
INSERT INTO `upms_role_permission` VALUES (664, 1, 25);
INSERT INTO `upms_role_permission` VALUES (665, 1, 3);
INSERT INTO `upms_role_permission` VALUES (666, 1, 8);
INSERT INTO `upms_role_permission` VALUES (667, 1, 9);
INSERT INTO `upms_role_permission` VALUES (668, 1, 17);
INSERT INTO `upms_role_permission` VALUES (669, 1, 100);
INSERT INTO `upms_role_permission` VALUES (670, 1, 101);
INSERT INTO `upms_role_permission` VALUES (671, 1, 102);
INSERT INTO `upms_role_permission` VALUES (672, 1, 104);
INSERT INTO `upms_role_permission` VALUES (673, 1, 105);
INSERT INTO `upms_role_permission` VALUES (674, 1, 106);
INSERT INTO `upms_role_permission` VALUES (675, 1, 4);
INSERT INTO `upms_role_permission` VALUES (676, 1, 7);
INSERT INTO `upms_role_permission` VALUES (677, 1, 23);
INSERT INTO `upms_role_permission` VALUES (678, 1, 24);
INSERT INTO `upms_role_permission` VALUES (679, 1, 26);
INSERT INTO `upms_role_permission` VALUES (680, 1, 5);
INSERT INTO `upms_role_permission` VALUES (681, 1, 19);
INSERT INTO `upms_role_permission` VALUES (682, 1, 20);
INSERT INTO `upms_role_permission` VALUES (683, 1, 27);
INSERT INTO `upms_role_permission` VALUES (684, 1, 6);
INSERT INTO `upms_role_permission` VALUES (685, 1, 21);
INSERT INTO `upms_role_permission` VALUES (686, 1, 22);
INSERT INTO `upms_role_permission` VALUES (687, 1, 28);
INSERT INTO `upms_role_permission` VALUES (688, 1, 15);
INSERT INTO `upms_role_permission` VALUES (689, 1, 16);
INSERT INTO `upms_role_permission` VALUES (690, 1, 29);
INSERT INTO `upms_role_permission` VALUES (691, 1, 30);
INSERT INTO `upms_role_permission` VALUES (692, 1, 31);

-- ----------------------------
-- Table structure for upms_system
-- ----------------------------
DROP TABLE IF EXISTS `upms_system`;
CREATE TABLE `upms_system`  (
  `system_id` bigint(10) UNSIGNED NOT NULL COMMENT '编号',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `banner` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '背景',
  `theme` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主题',
  `basepath` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '根目录',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态(-1:黑名单,1:正常)',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '系统名称',
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '系统标题',
  `description` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '系统描述',
  `ctime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `orders` bigint(20) NOT NULL COMMENT '排序',
  PRIMARY KEY (`system_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_system
-- ----------------------------
INSERT INTO `upms_system` VALUES (1, '', '', '#29A176', 'http://localhost/index', 1, 'ming-upms', '权限管理系统', '用户权限管理系统（RBAC细粒度用户权限、统一后台、单点登录、会话管理）', '2020-05-02 19:55:02', 1);
INSERT INTO `upms_system` VALUES (2, NULL, NULL, '#29A176', 'http://localhost/ming-oa', 1, 'ming-oa', '办公管理系统', '办公管理系统（主要负责办公人事、信息公告和流程管理）', '2020-05-02 19:55:02', 2);
INSERT INTO `upms_system` VALUES (4, '', '', '', 'http://localhost/ming-cms', 1, 'ming-cms', '内容管理系统', '内容管理系统（文件存储、知识共享）', '2020-05-05 18:23:30', 1);

-- ----------------------------
-- Table structure for upms_upload_file
-- ----------------------------
DROP TABLE IF EXISTS `upms_upload_file`;
CREATE TABLE `upms_upload_file`  (
  `system_id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所属用户\r\n',
  `basepath` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '根目录',
  `status` tinyint(4) NOT NULL COMMENT '状态(-1:黑名单,1:正常)',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件名称',
  `salt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `description` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '系统描述',
  `ctime` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `orders` bigint(20) NOT NULL COMMENT '排序',
  PRIMARY KEY (`system_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_upload_file
-- ----------------------------
INSERT INTO `upms_upload_file` VALUES (38, '1', '2021-05/44e1168c185340ecad71fef24d39303f0960724101e44b8992e6618b8bbe2335QQ__20210516170039.zip', -1, 'admin的论文2', '123', '9aed5f35f4ce2a56de677cad90190e96', '2021-06-03 02:27:13', 3);
INSERT INTO `upms_upload_file` VALUES (39, '100', '2021-05/99fbc69ae7794459be38408929c1e008capture_20201022131552566.bmp', -1, '20181101051的论文22', '123', '249719e45e2e31725dfc50db63335d9f', '2021-05-29 11:24:06', 3);
INSERT INTO `upms_upload_file` VALUES (40, '101', '2021-05/d0a3615fe5514281ad627d7228474071capture_20201022131552566.bmp', 1, 'teacher的论文', '456', '249719e45e2e31725dfc50db63335d9f', '2021-05-28 07:44:51', 3);

-- ----------------------------
-- Table structure for upms_user
-- ----------------------------
DROP TABLE IF EXISTS `upms_user`;
CREATE TABLE `upms_user`  (
  `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '帐号',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码MD5(密码+盐)',
  `salt` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '盐',
  `realname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
  `avatar` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
  `sex` tinyint(4) NOT NULL COMMENT '性别',
  `locked` tinyint(4) NULL DEFAULT NULL COMMENT '状态(0:正常,1:锁定)',
  `ctime` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `organization_id` int(10) NULL DEFAULT NULL COMMENT '所属组织机构',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 104 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_user
-- ----------------------------
INSERT INTO `upms_user` VALUES (1, 'admin', '1d45e9bc108bcf0237885d37df5488c9', '66f1b370c660445a8657bf8bf1794486', '管理员', '/img/profile_small01.jpg', '18271681269', '1090760001@qq.com', 1, 0, '2020-05-02 05:55:02', 1);
INSERT INTO `upms_user` VALUES (100, '20181101051', '7ab7a5752b43a25400a5a7d7d69b9280', 'yy2JNr86wAWB5m2AStBgOGyzoNihkH', '韩旭', '/img/profile_small03.jpg', '13012345678', '20181101051@hbu.com', 1, 0, '2021-05-27 15:10:36', 6);
INSERT INTO `upms_user` VALUES (101, 'teacher', '9a68a0bbc27f92a32de416e77aa0aed6', 'bdQ91zoHuRiJe8mjXYhzB8DVSTnWlX', '柴老师', '/img/profile_small03.jpg', '12345678', '12345678@hbu.cn', 0, 0, '2021-05-28 15:26:53', 2);
INSERT INTO `upms_user` VALUES (102, '20181101050', '0a459693c9793bad5ed848420980f873', 'eJYGp0rtxcCm9hkmWBpB0hCZnpNn2H', '韩军', '/img/profile_small03.jpg', '15112345678', '20181101050@hbu.com', 1, 0, '2021-05-29 16:42:10', 5);
INSERT INTO `upms_user` VALUES (103, '20181101052', '2d61f9494fb57eec2204ad8dde6a3456', 'lc1HqIZpmMD2gqNr4nMoUQG1PRh8U4', '李旭', '/img/profile_small03.jpg', '11213123', '3123123', 1, 0, '2021-05-29 16:43:04', 6);
INSERT INTO `upms_user` VALUES (105, '20181101053', '2d61f9494fb57eec2204ad8dde6a3456', 'lc1HqIZpmMD2gqNr4nMoUQG1PRh8U4', '张旭', '/img/profile_small03.jpg', '11213123', '3123123', 1, 0, '2021-05-29 16:43:04', 6);
INSERT INTO `upms_user` VALUES (106, '20181101054', '2d61f9494fb57eec2204ad8dde6a3456', 'lc1HqIZpmMD2gqNr4nMoUQG1PRh8U4', '王旭', '/img/profile_small03.jpg', '11213123', '3123123', 1, 0, '2021-05-29 16:43:04', 6);
INSERT INTO `upms_user` VALUES (107, '20181101055', '2d61f9494fb57eec2204ad8dde6a3456', 'lc1HqIZpmMD2gqNr4nMoUQG1PRh8U4', '杨旭', '/img/profile_small03.jpg', '11213123', '3123123', 1, 0, '2021-05-29 16:43:04', 6);

-- ----------------------------
-- Table structure for upms_user_organization
-- ----------------------------
DROP TABLE IF EXISTS `upms_user_organization`;
CREATE TABLE `upms_user_organization`  (
  `user_organization_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` int(10) UNSIGNED NOT NULL COMMENT '用户编号',
  `organization_id` int(10) UNSIGNED NOT NULL COMMENT '组织编号',
  PRIMARY KEY (`user_organization_id`) USING BTREE,
  INDEX `FK_uuo_user_id`(`user_id`) USING BTREE,
  INDEX `FK_uuo_organization_id`(`organization_id`) USING BTREE,
  CONSTRAINT `FK_uuo_organization_id` FOREIGN KEY (`organization_id`) REFERENCES `upms_organization` (`organization_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_uuo_user_id` FOREIGN KEY (`user_id`) REFERENCES `upms_user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户组织关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_user_organization
-- ----------------------------

-- ----------------------------
-- Table structure for upms_user_role
-- ----------------------------
DROP TABLE IF EXISTS `upms_user_role`;
CREATE TABLE `upms_user_role`  (
  `user_role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` int(10) UNSIGNED NOT NULL COMMENT '用户编号',
  `role_id` int(10) UNSIGNED NOT NULL COMMENT '角色编号',
  PRIMARY KEY (`user_role_id`) USING BTREE,
  INDEX `FK_uur_user_id`(`user_id`) USING BTREE,
  INDEX `FK_uur_role_id`(`role_id`) USING BTREE,
  CONSTRAINT `FK_uur_role_id_1` FOREIGN KEY (`role_id`) REFERENCES `upms_role` (`role_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_uur_user_id` FOREIGN KEY (`user_id`) REFERENCES `upms_user` (`user_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of upms_user_role
-- ----------------------------
INSERT INTO `upms_user_role` VALUES (1, 1, 1);
INSERT INTO `upms_user_role` VALUES (2, 1, 2);
INSERT INTO `upms_user_role` VALUES (19, 1, 6);
INSERT INTO `upms_user_role` VALUES (100, 100, 101);
INSERT INTO `upms_user_role` VALUES (101, 101, 100);
INSERT INTO `upms_user_role` VALUES (102, 107, 101);
INSERT INTO `upms_user_role` VALUES (103, 106, 101);
INSERT INTO `upms_user_role` VALUES (104, 105, 101);
INSERT INTO `upms_user_role` VALUES (105, 103, 101);
INSERT INTO `upms_user_role` VALUES (106, 102, 101);

SET FOREIGN_KEY_CHECKS = 1;
