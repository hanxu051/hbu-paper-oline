package com.ming.admin.controller.system.upms;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ming.admin.service.FileService;
import com.ming.common.utils.*;
import com.ming.upms.system.domain.UpmsUploadFileDO;
import com.ming.upms.system.domain.UpmsUserDO;
import com.ming.upms.system.service.UpmsPermissionService;
import com.ming.upms.system.service.UpmsStuUploadFileService;
import com.ming.upms.system.service.UpmsUploadFileService;
import com.ming.upms.system.service.UpmsUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.ming.upms.common.util.ShiroUtils.getUser;


/**
 * 系统
 *
 * @author HANXU
 * @email 1076998404@qq.com
 * @date 2021-05-26 21:09:30
 */

@Controller
@RequestMapping("/system/upmsStuUploadFile")
public class UpmsStuUploadFileController {
    private static final Logger logger = LoggerFactory.getLogger(UpmsStuUploadFileController.class);

    @Autowired
    private UpmsUserService upmsUserService;

    @Autowired
    private UpmsPermissionService upmsPermissionService;
    @Autowired
    private UpmsStuUploadFileService upmsUploadFileService;
    @Autowired
    private FileService fileService;

    @GetMapping()
    @RequiresPermissions("system:upmsUploadFile:read")
    String UpmsUploadFile() {
        return "system/upmsStuUploadFile/upmsUploadFile";
    }

    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("system:upmsUploadFile:read")
    public PageUtils list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        params.put("userId",getUser().getUserId().toString());
        Query query = new Query(params);
        List<UpmsUploadFileDO> upmsUploadFileList = upmsUploadFileService.list(query);
        int total = upmsUploadFileService.count(query);
        PageUtils pageUtils = new PageUtils(upmsUploadFileList, total);
        return pageUtils;
    }

    @GetMapping("/add")
    @RequiresPermissions("system:upmsUploadFile:add")
    String add(Model model) {
        //获取用户资源
        UpmsUserDO user = getUser();
        model.addAttribute("user", user);
        return "system/upmsStuUploadFile/add";
    }

    @GetMapping("/edit/{systemId}")
    @RequiresPermissions("system:upmsUploadFile:edit")
    String edit(@PathVariable("systemId") Long systemId, Model model) {
        UpmsUploadFileDO upmsUploadFile = upmsUploadFileService.get(systemId);
        model.addAttribute("upmsUploadFile", upmsUploadFile);
        //获取用户资源
        UpmsUserDO user = getUser();
        model.addAttribute("user", user);
        return "system/upmsStuUploadFile/edit";
    }

    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    @RequiresPermissions("system:upmsUploadFile:add")
    public R save(UpmsUploadFileDO upmsUploadFile) {
        if (upmsUploadFileService.save(upmsUploadFile) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping("/update")
    @RequiresPermissions("system:upmsUploadFile:edit")
    public R update(UpmsUploadFileDO upmsUploadFile,@RequestParam("system_id") Long systemId) {
        upmsUploadFile.setSystemId(systemId);
        upmsUploadFileService.update(upmsUploadFile);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @ResponseBody
    @RequiresPermissions("system:upmsUploadFile:remove")
    public R remove(Long systemId) {
        if (upmsUploadFileService.remove(systemId) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 删除
     */
    @PostMapping("/batchRemove")
    @ResponseBody
    @RequiresPermissions("system:upmsUploadFile:batchRemove")
    public R remove(@RequestParam("ids[]") Long[] systemIds) {
        upmsUploadFileService.batchRemove(systemIds);
        return R.ok();
    }

    //单一文件上传
    @RequestMapping("/fileupload")
    @RequiresPermissions("system:upmsUploadFile:add")
    @ResponseBody
    public JSON uploadFile(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request, Model model) throws FileNotFoundException {
        JSONObject json = new JSONObject();

        String msg = "";
        String key = request.getParameter("key");


        String realPath = ResourceUtils.getURL("classpath:").getPath();
        String time = "";
        String Path = "";
        String NewfileName = "";
        String fileMD5 = "";
        try {
            if (file.isEmpty()) {
                model.addAttribute("msg", "上传失败，请选择文件！");

                return json;
            }
            String filename = file.getOriginalFilename();
            //String filePath = request.getServletContext().getRealPath("/upload");
            String filePath = ResourceUtils.getURL("classpath:").getPath() + "static/";
            //避免文件重复覆盖
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            //时间戳分类文件
            time = new SimpleDateFormat("YYYY-MM").format(new Date());
            Path = filePath + time + "/";
            realPath = filePath + time + "/" + uuid + filename;
            NewfileName = uuid + filename;
            msg = time + "/" + uuid + filename;
            File dest = new File(realPath);
            //检测是否存在目录，无，则创建
            if (!dest.getParentFile().exists()) {
                dest.getParentFile().mkdirs();//新建文件夹 多级目录
            }
            file.transferTo(dest);//文件写入
            fileMD5 = MD5Utils.getMD5(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 加密文件
        try {
            String ZipName = NewfileName.substring(0, NewfileName.lastIndexOf('.'))+"new";
            FileUtil.zipFilesAndEncrypt(Path, NewfileName, ZipName, key);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 将src路径发送至html页面
//        model.addAttribute("filename", msg);
        //获取用户资源
        UpmsUserDO user = getUser();
//        model.addAttribute("user", user);
//        model.addAttribute("Key", key);
//        model.addAttribute("fileMD5", fileMD5);
        json.put("user", user);
        json.put("Key", key);
        json.put("fileMD5", fileMD5);
        json.put("filename", msg);
        return json;
    }


    @PostMapping("/filedownload")
    public ResponseEntity<Resource> downloadFile(@RequestParam("filePath") String filePath, @RequestParam("status") int status, @RequestParam("uping") int uping, @RequestParam("Key") String key, @RequestParam("MD52") String MD52, HttpServletRequest request) throws FileNotFoundException {
        // Load file as Resource
        filePath = DecodeUtil.decode(filePath);
        key = DecodeUtil.decode(key);
        MD52 = DecodeUtil.decode(MD52);

        String pathRoot = ResourceUtils.getURL("classpath:").getPath().substring(1) + "static/";
        String fileName = pathRoot + filePath;
        if (status == 1) {
            fileName = fileName.substring(0, fileName.lastIndexOf('.')) + "new.zip";
        }


        File dest = new File(fileName);
        String fileMD5 = MD5Utils.getMD5(dest);

        if (fileMD5.equals(MD52)) {

        }
        if (uping != 1) {
            UpmsUploadFileDO uploadFileDO = upmsUploadFileService.getBybasepath(filePath);
            if (!uploadFileDO.getSalt().equals(key)) {
                fileName = pathRoot + "/passerror.txt";
            }
        }

        Resource resource = fileService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            System.out.println(ex);
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .header("filename", resource.getFilename())
                .body(resource);
    }


    //http://localhost:8083/system/upmsUploadFile/verify
    //单一文件上传
    @RequestMapping("/verify")
    @ResponseBody
    public JSON verify(@RequestParam(value = "uping") int uping,@RequestParam(value = "status") int status, @RequestParam(value = "MD53") String localMD5, @RequestParam(value = "MD54") String srcMD5,@RequestParam(value = "MD55") String zipMD5, @RequestParam(value = "basepath") String basepath, @RequestParam(value = "Key") String Key, HttpServletRequest request, Model model) throws FileNotFoundException {
        JSONObject json = new JSONObject();
        if(uping==1&&status==1){// 云端验证 加密文件
            String pathRoot = ResourceUtils.getURL("classpath:").getPath().substring(1) + "static/";
            String fileName = pathRoot + basepath;
            if (status == 1) {
                fileName = fileName.substring(0, fileName.lastIndexOf('.')) + "new.zip";
            }

            File dest = new File(fileName);
            String fileMD5 = MD5Utils.getMD5(dest);
            if(fileMD5.equals(localMD5)){
                json.put("success","文件验证正确");
                json.put("zipMD5",fileMD5);
                return json;
            }else{
                json.put("error","密码错误或文件不一致");
                return json;
            }
        }

        UpmsUploadFileDO upmsUploadFile = upmsUploadFileService.getBybasepath(basepath);
        if (!upmsUploadFile.getSalt().equals(Key)){
            json.put("error","密码错误");
            return json;
        }else{
            if(upmsUploadFile.getDescription()==srcMD5){
                json.put("success","文件验证正确");
            }else{
                json.put("error","密码错误或文件不一致");
            }
        }
        return json;
    }

}
