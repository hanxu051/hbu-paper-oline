package com.ming.admin.controller.system.upms;

import java.io.*;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ming.admin.service.FileService;
import com.ming.upms.system.domain.UpmsOrganizationDO;
import com.ming.upms.system.domain.UpmsUploadFileVO;
import com.ming.upms.system.service.UpmsOrganizationService;
import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;
import org.springframework.core.io.Resource;

import com.ming.common.utils.*;
import com.ming.upms.system.domain.UpmsUserDO;
import com.ming.upms.system.service.UpmsPermissionService;
import com.ming.upms.system.service.UpmsUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ming.upms.system.domain.UpmsUploadFileDO;
import com.ming.upms.system.service.UpmsUploadFileService;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

import static com.ming.upms.common.util.ShiroUtils.getUser;


/**
 * 系统
 *
 * @author HANXU
 * @email 1076998404@qq.com
 * @date 2021-05-26 21:09:30
 */

@Controller
@RequestMapping("/system/upmsUploadFile")
public class UpmsUploadFileController {
    private static final Logger logger = LoggerFactory.getLogger(UpmsUploadFileController.class);

    @Autowired
    private UpmsUserService upmsUserService;

    @Autowired
    private UpmsPermissionService upmsPermissionService;
    @Autowired
    private UpmsUploadFileService upmsUploadFileService;
    @Autowired
    private FileService fileService;
    @Autowired
    private UpmsOrganizationService upmsOrganizationService;

    @GetMapping()
    @RequiresPermissions("system:upmsUploadFile:read")
    String UpmsUploadFile() {
        return "system/upmsUploadFile/upmsUploadFile";
    }

    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("system:upmsUploadFile:read")
    public PageUtils list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);
        List<UpmsUploadFileDO> upmsUploadFiles = upmsUploadFileService.list(query);
        List<UpmsUploadFileVO> upmsUploadFileList = new ArrayList();

        for (UpmsUploadFileDO upmsUploadFileDO : upmsUploadFiles) {
            UpmsUploadFileVO upmsUploadFileVO = new UpmsUploadFileVO(upmsUploadFileDO);
            UpmsUserDO upmsUserDO = upmsUserService.get(Long.parseLong(upmsUploadFileDO.getUserId()));
            upmsUploadFileVO.setUsername( upmsUserDO.getUsername());
            upmsUploadFileVO.setRealname( upmsUserDO.getRealname());
            UpmsOrganizationDO upmsOrganizationDO = upmsOrganizationService.getOrganizationById(upmsUserDO.getOrganizationId());
            upmsUploadFileVO.setOrganizationName(upmsOrganizationDO.getName());
            upmsUploadFileVO.setOrganizationId( upmsOrganizationDO.getOrganizationId());
            upmsUploadFileList.add(upmsUploadFileVO);
        }
        int total = upmsUploadFileService.count(query);
        PageUtils pageUtils = new PageUtils(upmsUploadFileList, total);
        return pageUtils;
    }

    @GetMapping("/add")
    @RequiresPermissions("system:upmsUploadFile:add")
    String add(Model model) {
        //获取用户资源
        UpmsUserDO user = getUser();
        model.addAttribute("user", user);
        return "system/upmsUploadFile/add";
    }

    @GetMapping("/edit/{systemId}")
    @RequiresPermissions("system:upmsUploadFile:edit")
    String edit(@PathVariable("systemId") Long systemId, Model model) {
        UpmsUploadFileDO upmsUploadFile = upmsUploadFileService.get(systemId);
        model.addAttribute("upmsUploadFile", upmsUploadFile);
        //获取用户资源
        UpmsUserDO user = getUser();
        model.addAttribute("user", user);
        return "system/upmsUploadFile/edit";
    }

    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    @RequiresPermissions("system:upmsUploadFile:add")
    public R save(UpmsUploadFileDO upmsUploadFile) {
        if (upmsUploadFileService.save(upmsUploadFile) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping("/update")
    @RequiresPermissions("system:upmsUploadFile:edit")
    public R update(UpmsUploadFileDO upmsUploadFile, @RequestParam("system_id") Long systemId) {
        upmsUploadFile.setSystemId(systemId);
        upmsUploadFileService.update(upmsUploadFile);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @ResponseBody
    @RequiresPermissions("system:upmsUploadFile:remove")
    public R remove(Long systemId) {
        if (upmsUploadFileService.remove(systemId) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 删除
     */
    @PostMapping("/batchRemove")
    @ResponseBody
    @RequiresPermissions("system:upmsUploadFile:batchRemove")
    public R remove(@RequestParam("ids[]") Long[] systemIds) {
        upmsUploadFileService.batchRemove(systemIds);
        return R.ok();
    }

    //单一文件上传
    @RequestMapping("/fileupload")
    @RequiresPermissions("system:upmsUploadFile:add")
    @ResponseBody
    public JSON uploadFile(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request, Model model) throws FileNotFoundException {
        JSONObject json = new JSONObject();

        String msg = "";
        String key = request.getParameter("key");


        String realPath = ResourceUtils.getURL("classpath:").getPath();
        String time = "";
        String Path = "";
        String NewfileName = "";
        String fileMD5 = "";
        try {
            if (file.isEmpty()) {
                model.addAttribute("msg", "上传失败，请选择文件！");

                return json;
            }
            String filename = file.getOriginalFilename();
            //String filePath = request.getServletContext().getRealPath("/upload");
            String filePath = ResourceUtils.getURL("classpath:").getPath() + "static/";
            //避免文件重复覆盖
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            //时间戳分类文件
            time = new SimpleDateFormat("YYYY-MM").format(new Date());
            Path = filePath + time + "/";
            realPath = filePath + time + "/" + uuid + filename;
            NewfileName = uuid + filename;
            msg = time + "/" + uuid + filename;
            File dest = new File(realPath);
            //检测是否存在目录，无，则创建
            if (!dest.getParentFile().exists()) {
                dest.getParentFile().mkdirs();//新建文件夹 多级目录
            }
            file.transferTo(dest);//文件写入
            fileMD5 = MD5Utils.getMD5(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 加密文件
        try {
            String ZipName = NewfileName.substring(0, NewfileName.lastIndexOf('.')) + "new";
            FileUtil.zipFilesAndEncrypt(Path, NewfileName, ZipName, key);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 将src路径发送至html页面
//        model.addAttribute("filename", msg);
        //获取用户资源
        UpmsUserDO user = getUser();
//        model.addAttribute("user", user);
//        model.addAttribute("Key", key);
//        model.addAttribute("fileMD5", fileMD5);
        json.put("user", user);
        json.put("Key", key);
        json.put("fileMD5", fileMD5);
        json.put("filename", msg);
        return json;
    }


    @PostMapping("/filedownload")
    public ResponseEntity<Resource> downloadFile(@RequestParam("filePath") String filePath, @RequestParam("status") int status, @RequestParam("uping") int uping, @RequestParam("Key") String key, @RequestParam("MD52") String MD52, HttpServletRequest request) throws FileNotFoundException {
        // Load file as Resource
        filePath = DecodeUtil.decode(filePath);
        key = DecodeUtil.decode(key);
        MD52 = DecodeUtil.decode(MD52);

        String pathRoot = ResourceUtils.getURL("classpath:").getPath().substring(1) + "static/";
        String fileName = pathRoot + filePath;
        if (status == 1) {
            fileName = fileName.substring(0, fileName.lastIndexOf('.')) + "new.zip";
        }


        File dest = new File(fileName);
        String fileMD5 = MD5Utils.getMD5(dest);

        if (fileMD5.equals(MD52)) {

        }
        if (uping != 1) {
            UpmsUploadFileDO uploadFileDO = upmsUploadFileService.getBybasepath(filePath);
            if (!uploadFileDO.getSalt().equals(key)) {
                fileName = pathRoot + "/passerror.txt";
            }
        }

        Resource resource = fileService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            System.out.println(ex);
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .header("filename", resource.getFilename())
                .body(resource);
    }


    //http://localhost:8083/system/upmsUploadFile/verify
    //单一文件上传
    @RequestMapping("/verify")
    @ResponseBody
    public JSON verify(@RequestParam(value = "uping") int uping, @RequestParam(value = "status") int status, @RequestParam(value = "MD53") String localMD5, @RequestParam(value = "MD54") String srcMD5, @RequestParam(value = "MD55") String zipMD5, @RequestParam(value = "basepath") String basepath, @RequestParam(value = "Key") String Key, HttpServletRequest request, Model model) throws FileNotFoundException {
        JSONObject json = new JSONObject();
        if (uping == 1 && status == 1) {// 云端验证 加密文件
            String pathRoot = ResourceUtils.getURL("classpath:").getPath().substring(1) + "static/";
            String fileName = pathRoot + basepath;
            if (status == 1) {
                fileName = fileName.substring(0, fileName.lastIndexOf('.')) + "new.zip";
            }

            File dest = new File(fileName);
            String fileMD5 = MD5Utils.getMD5(dest);
            if (fileMD5.equals(localMD5)) {
                json.put("success", "文件验证正确");
                json.put("zipMD5", fileMD5);
                return json;
            } else {
                json.put("error", "密码错误或文件不一致");
                return json;
            }
        }

        UpmsUploadFileDO upmsUploadFile = upmsUploadFileService.getBybasepath(basepath);
        if (!upmsUploadFile.getSalt().equals(Key)) {
            json.put("error", "密码错误");
            return json;
        } else {
            if (upmsUploadFile.getDescription() == srcMD5) {
                json.put("success", "文件验证正确");
            } else {
                json.put("error", "密码错误或文件不一致");
            }
        }
        return json;
    }

}
