$(function () {
    load();
});


$().ready(function () {
    validateRule();
    $('#save').bind('click', function save() {
            //  alert("点击了 保存");
            $.ajax({
                cache: true,
                type: "POST",
                url: "/system/upmsUploadFile/save",
                data: $('#signupForm').serialize(),// 你的formid
                async: false,
                error: function (request) {
                    parent.layer.alert("Connection error");
                },
                success: function (data) {
                    if (data.code == 0) {
                        parent.layer.msg("操作成功");
                        //parent.reLoad();
                        var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(index);

                    } else {
                        parent.layer.alert(data.msg)
                    }

                }
            });

        }
    );

    $('#MD5ve').bind('click', function verify() {
            //alert("点击了 验证");
            var params = $('#verifyfrom').serialize();
            console.log($('#verifyfrom').serialize());
            console.log(urlToObj(params))
            params = urlToObj(params);
            console.log(params.uping==1)
            console.log(params.uping)
            console.log(params.status==-1)
            console.log(params.status)
            if(params.uping==1 && params.status==-1){
                console.log("文件未保存 直接本地校验");
                if(params.MD53==params.MD54){
                    alert("本地校验正确");
                }else{
                    alert("本地校验失败 文件 MD5 不一致")
                }
            }else{
                $.ajax({
                    cache: true,
                    type: "POST",
                    url: "/system/upmsUploadFile/verify",
                    data: $('#verifyfrom').serialize(),// 你的formid
                    async: false,
                    error: function (request) {
                        alert("Connection error");
                    },
                    success: function (data) {
                        console.log(data);
                        alert(data.error+data.success)
                        if(data.err != undefined){
                            alert(data.err);
                        }else{
                            alert(data.success);

                            $("#MD55").attr("value", data.zipMD5);
                        }
                    }
                });
            }

        }
    );

    function urlToObj(str) {
        var obj = {};

        var arr2 = str.split("&");
        for (var i = 0; i < arr2.length; i++) {
            var res = arr2[i].split("=");
            obj[res[0]] = res[1];
        }
        return obj;
    }

    function postFile(params, url) { //params是post请求需要的参数，url是请求url地址
        var form = document.createElement("form");
        form.style.display = 'none';
        form.action = url;
        form.method = "post";
        document.body.appendChild(form);

        for (var key in params) {
            var input = document.createElement("input");
            input.type = "hidden";
            input.name = key;
            input.value = params[key];
            form.appendChild(input);
        }
        form.submit();
        form.remove();
    }

    $('#download').bind('click', function download() {
        var params = $('#downloadForm').serialize();
        console.log($('#downloadForm').serialize());
        console.log(urlToObj(params))
        params = urlToObj(params);
        if(params.Key!= $('#Key').val()){
            alert("密码错误")
        }else{
            postFile(params , "/system/upmsUploadFile/filedownload");
        }
        // params.filePath='2018-08-16';
        // params.uping=0;
        // params.status='12924,12925,12926';
        // params.Key='332584,12927,333950,314216';

        // // alert("点击了 下载");
        //  $.ajax({
        //      cache: true,
        //      type: "POST",
        //      url: "/system/upmsUploadFile/filedownload",
        //      data: $('#downloadForm').serialize(),// 你的formid
        //      async: false,
        //      error: function (request) {
        //          parent.layer.alert("请检查文件密码");
        //      },
        //      success: function (data ,textStatus,request) {
        //          // console.log(data)
        //          // console.log(request);
        //          // var filename = request.getResponseHeader("filename");
        //          // console.log(filename)
        //          // var url = window.URL.createObjectURL(new Blob([data]))
        //          // var a = document.createElement('a')
        //          // a.setAttribute("download",filename)
        //          // a.href = url
        //          // a.click();
        //
        //          if (data.code == 0) {
        //              parent.layer.msg("操作成功");
        //              parent.reLoad();
        //              var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
        //              parent.layer.close(index);
        //          } else {
        //              parent.layer.alert(data.msg)
        //          }
        //
        //      }
        //  });

    });

    // $('#upload').bind('click', function upload() {
    //     alert("点击了 上传");
    //     var data = $('#uploadForm').serialize();
    //     console.log(data);
    //     var formData = new FormData($("#uploadForm")[0]);
    //     console.log(formData);
    //     var files = $('#file00').val();
    //     console.log(files);
    //     formData.append("file00",files);
    //     console.log(formData);
    //
    //     $.ajax({
    //         cache: true,
    //         type: "POST",
    //         url: "/system/upmsUploadFile/fileupload",
    //         //enctype="multipart/form-data"
    //         data: formData,
    //         async: false,
    //         error: function (request) {
    //             parent.layer.alert("Connection error");
    //         },
    //         success: function (data) {
    //             if (data.code == 0) {
    //                 parent.layer.msg("操作成功");
    //                 parent.reLoad();
    //                 var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
    //                 parent.layer.close(index);
    //             } else {
    //                 parent.layer.alert(data.msg)
    //             }
    //
    //         }
    //     });
    //
    // });
});

// $.validator.setDefaults({
//     submitHandler: function () {
//         save();
//     }
// });

function validateRule() {
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        rules: {
            name: {
                required: true
            }
        },
        messages: {
            name: {
                required: icon + "请输入姓名"
            }
        }
    })
}

document.querySelector('#md5file').addEventListener('change', e => {
    var file = e.target.files[0];
    var fileReader = new FileReader();
    fileReader.readAsBinaryString(file);
    fileReader.onload = e =>{
        var md5 = SparkMD5.hashBinary(e.target.result);
        $("#MD53").attr("value", md5);
        console.log(md5);

    }
    })
;
