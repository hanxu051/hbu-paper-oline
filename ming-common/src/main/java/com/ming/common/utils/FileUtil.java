package com.ming.common.utils;

import com.alibaba.druid.support.logging.SLF4JImpl;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.UUID;
import java.util.zip.ZipOutputStream;

public class FileUtil {
    private static final Logger log = LoggerFactory.getLogger(FileUtil.class);
	public static void uploadFile(byte[] file, String filePath, String fileName) throws Exception {
		File targetFile = new File(filePath);
		if (!targetFile.exists()) {
			targetFile.mkdirs();
		}
		FileOutputStream out = new FileOutputStream(filePath + fileName);
		out.write(file);
		out.flush();
		out.close();
	}

	public static boolean deleteFile(String fileName) {
		File file = new File(fileName);
		// 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
		if (file.exists() && file.isFile()) {
			if (file.delete()) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public static String renameToUUID(String fileName) {
		return UUID.randomUUID() + "." + fileName.substring(fileName.lastIndexOf(".") + 1);
	}

    /**
     *
     * @Title: zipFilesAndEncrypt
     * @Description: 将指定路径下的文件压缩至指定zip文件，并以指定密码加密,若密码为空，则不进行加密保护
     * @param srcFileName 待压缩文件路径
     * @param zipFileName zip文件名
     * @param password 加密密码
     * @return
     * @throws Exception
     */
    public static void zipFilesAndEncrypt(String Path,String srcFileName,String zipFileName,String password) throws Exception{

        ZipOutputStream outputStream=null;
        String srcFilePath = Path+srcFileName;
        System.out.println("进入测试类");
        if(StringUtils.isEmpty(Path) || StringUtils.isEmpty(Path)){
            log.error("请求的压缩路径或者文件名有误");
            return;
        }
        try {
            ZipParameters parameters = new ZipParameters();
            parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
            parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
            if(!StringUtils.isEmpty(password)){
                parameters.setEncryptFiles(true);
                parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
                parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
                parameters.setPassword(password);
            }
            ArrayList<File> filesToAdd = new ArrayList<File>();
            File file=new File(srcFilePath);
            File[] files = new File[0];
            if(file.isDirectory())
            {
                files = file.listFiles();
                for(int i=0;i<files.length;i++){
                    filesToAdd.add(new File(srcFilePath+files[i].getName()));
                    System.out.println("文件名称："+files[i].getName());
                }
            }
            else {
                filesToAdd.add(new File(srcFilePath));
            }
            ZipFile zipFile = new ZipFile(Path+zipFileName+".zip");
            zipFile.addFiles(filesToAdd, parameters);
        }
        catch (Exception e) {
            System.out.println("文件压缩出错");
            log.error("文件压缩出错", e);
            throw e;
        }
    }
}
